package com.training.pets;

public class Pet {

    private static int numPets = 0;

    private int numLegs;
    private String name;

    public Pet() {
        numPets++;
    }

    public int getNumLegs() {
        return numLegs;
    }

    public void setNumLegs(int numLegs) {
        this.numLegs = numLegs;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void feed() {
        System.out.println("Feed generic pet some generic pet food");
    }

    public static int getNumPets() {
        return numPets;
    }

}
