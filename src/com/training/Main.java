package com.training;

import com.training.pets.Dragon;
import com.training.pets.Pet;

public class Main {

    public static void main(String[] args) {

        int myVar = 10;

        long myVarLong = 10;

        myVarLong = myVar;

        String myString = "Hello everyone";

        myString = null;

        // ---------------------------------------

        Pet myPet = new Pet();
        System.out.println(myPet.getNumPets());

        myPet.feed();
        myPet.setName("Mycroft");
        myPet.setNumLegs(4);

        System.out.println(myPet.getName());
        System.out.println(myPet.getNumLegs());

        // ---------------------------------------

        Dragon myDragon = new Dragon("Mycroft");

        myDragon.feed("goats");
        //myDragon.setName("Mycroft");
        myDragon.breatheFire();

        System.out.println(myDragon.getName());
        System.out.println(myDragon.getNumLegs());

        // ---------------------------------------

        Pet myOtherDragon = new Dragon();

        myOtherDragon.feed();
        myOtherDragon.setName("Mycroft");
        ((Dragon)myOtherDragon).breatheFire();

        System.out.println(myOtherDragon.getName());
        System.out.println(myOtherDragon.getNumLegs());

    }

}
