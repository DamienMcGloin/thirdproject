package com.training;

import com.training.pets.Pet;

import java.util.ArrayList;

public class ArraysMain {

    public static void main(String[] args) {
        ArrayList<Pet> myPetList = new ArrayList<Pet>();

        int[] intArray = {5, 32, 64, 32, 77};

        Pet[] petArray = new Pet[10];

        for(int i=0; i< petArray.length; i++) {
            System.out.println(petArray[i]);
        }

    }

}
