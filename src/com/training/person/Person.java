package com.training.person;

import com.training.Feedable;

public class Person implements Feedable {

    private String name;

    public Person() {

    }

    public Person(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                '}';
    }

    @Override
    public void feed() {
        System.out.println("Time to put food in your mouth!");
    }
}
