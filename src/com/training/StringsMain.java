package com.training;

public class StringsMain {

    public static void main(String[] args) {

        String bigString = "start: ";

        StringBuilder sb = new StringBuilder();

        sb.append(bigString);

        for(int i=0; i<10; i++) {
            sb.append(i);
        }

        System.out.println(sb);

        String testString = "hello" + " " + "world" + " " + "after" + " " + "lunch";
    }

}
